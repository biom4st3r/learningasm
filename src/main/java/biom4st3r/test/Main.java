package biom4st3r.test;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
// https://asm.ow2.io/asm4-guide.pdf
public class Main implements Opcodes {

    private static final class VersionChangeCV extends ClassVisitor {
		private VersionChangeCV(int api, ClassVisitor classVisitor) {
			super(api, classVisitor);
		}

		@Override
		public void visit(int version, int access, String name, String signature, String superName,
		        String[] interfaces) {
		    // Now all visited classes with be branded as Java 1.1
		    super.visit(V1_1, access, name, signature, superName, interfaces);
		}
	}
	static interface Catchable 
    {
        void run() throws Throwable;
    }
    public static void main(String[] args) {
        Catchable c = ()->
        {
            byte[] clazzBytes = generateClass();
            transformClass(clazzBytes);


            
        }; try {c.run();} catch (Throwable e) {e.printStackTrace();}
        
    }
    static class BClassLoader extends ClassLoader
    {
        public Class<?> defineClass0(String name, byte[] b, int off, int len,
        ProtectionDomain protectionDomain)
        {
            return super.defineClass(name.replace("/", "\\."), b, off, len, protectionDomain);
        }
    }
    static Class<?> loadClass(String name,byte[] clazz)
    {
        return new BClassLoader().defineClass0("biom4st3r.test.Asm", clazz, 0, clazz.length, null);

    }

    static byte[] generateClass() throws InstantiationException, IllegalAccessException
    {
        ClassWriter cw = new ClassWriter(0);
        cw.visit(V1_8, ACC_STATIC|ACC_PUBLIC|ACC_FINAL, "biom4st3r/test/Asm", null, "java/lang/Object", new String[] {"java/util/RandomAccess"});
        // MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
        // mv.visitLocalVariable("this", "Lbiom4st3r/test/Asm", null, null, null, 0);
        // mv.visitLabel(new Label());
        // mv.visitVarInsn(ALOAD, 0);
        // mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V",false);
        // mv.visitLabel(new Label());
        // mv.visitInsn(RETURN);
        // mv.visitEnd();
        cw.visitField(ACC_PUBLIC|ACC_FINAL|ACC_STATIC, "LESS", "I", null, new Integer(19)).visitEnd();
        return cw.toByteArray();
    }

    static void transformClassBasic(byte[] clazz)
    {
        // The first step is to direct the events produced by a ClassReader to a ClassWriter
        ClassWriter cw = new ClassWriter(0);
        ClassReader cr = new ClassReader(clazz);
        cr.accept(cw,0);
        byte[] newClazz = cw.toByteArray();
    }
    static void transformClass(byte[] clazz)
    {
        // ClassReader -> ClassVisitor -> ClassWriter;
        Stopwatch sw = Stopwatch.createStarted();
        ClassWriter cw = new ClassWriter(0);
        
        ClassVisitor cv = new VersionChangeCV(ASM4, cw);

        ClassReader cr = new ClassReader(clazz);
        cr.accept(cv,0);
        byte[] newClazz = cw.toByteArray();
        System.out.println(sw.stop().elapsed().toMillis());
        
    }

    public static void premain(String agentArgs, Instrumentation inst)
    {
        inst.addTransformer(new ClassFileTransformer(){
            @Override
            public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                    ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
                ClassReader cr = new ClassReader(classfileBuffer);
                ClassWriter cw = new ClassWriter(cr,0);
                ClassVisitor vc = new VersionChangeCV(ASM4, cw);
                cr.accept(vc, 0);
                return cw.toByteArray();
            }
        });

    }
    
}